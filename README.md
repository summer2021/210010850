# 工作说明

根据说明文档，分别对项目进行了在线部署和离线部署，部署过程中发现了一些问题，并对所发现的问题文档进行修改。
本次项目分为两个阶段，第一阶段主要是学习相关内容，提出几个问题；第二阶段进行部署，提出问题，并作了对应的改进。

## 提出的问题
issues中是第一阶段所提出的问题；issues-2.txt中是第二阶段所提出的问题。


## 做出的改进
针对以上提出的问题，做了以下改进：

1) 修改以及整理eggo中的 README.md，以及对应的英文 README.en.md。
2) 修改以及整理eggo中/docs/manual.md。修改包括文档的逻辑以及其中存在的错误，以及说明不明确的地方。
3) 添加eggo中/docs/deploy_problems.md和/docs/configuration_file_description.md。
4) 修改eggo中的/config/README.md。
